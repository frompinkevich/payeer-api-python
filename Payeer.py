import requests

from urllib.parse import quote_plus

class PayeerAPI(object):
    url = 'https://payeer.com/ajax/api/api.php'
    agent = 'Mozilla/5.0 (Windows NT 6.1; rv:12.0) Gecko/20100101 Firefox/12.0'
    auth = {}
    output_data = None
    errors = None
    language = 'ru'

    def __init__(self, account, api_id, api_key):
        auth_data = {'account': account, 'apiId': api_id, 'apiPass': api_key}
        response = self.get_response(auth_data)
        if response.get('errors'):
            self.errors = response['errors']
        if response['auth_error'] == '0':
            self.auth = auth_data

    def get_response(self, post_data):
        post_data.update(self.auth)
        data = []
        for k, v in post_data.items():
            data.append(quote_plus(k) + '=' + quote_plus(v))
        data.append('language=' + self.language)
        data = '&'.join(data)
        resp = requests.post(self.url, data=data, headers={'User-Agent': self.agent,
                                                           'Content-Type': 'application/x-www-form-urlencoded'})
        return resp.json()

    def is_auth(self):
        if self.auth:
            return True
        return False

    def get_pay_systems(self):
        return self.get_response({'action': 'getPaySystems'})

    def get_balance(self):
        return self.get_response({'action': 'balance'})

    def get_errors(self):
        return self.errors

    def init_output(self, output_data):
        output_data.update({'action': 'initOutput'})
        resp = self.get_response(output_data)
        if not resp.get('errors'):
            self.output_data = output_data
            return True
        else:
            self.errors = resp['errors']
        return False

    def output(self):
        output_data = self.output_data
        output_data.update({'action': 'output'})
        resp = self.get_response(output_data)
        if not resp.get('errors'):
            return resp['historyId']
        else:
            self.errors = resp['errors']
        return False

    def get_history_info(self, history_id):
        return self.get_response({'action': 'historyInfo', 'historyId': history_id})

    def transfer(self, post_data):
        post_data.update({'action': 'transfer'})
        return self.get_response(post_data)

    def set_lang(self, language='en'):
        self.language = language
        return self

    def get_shop_order_info(self, post_data):
        post_data.update({'action': 'shopOrderInfo'})
        return self.get_response(post_data)

    def check_user(self, post_data):
        post_data.update({'action': 'checkUser'})
        resp = self.get_response(post_data)
        if not resp.get('errors'):
            return True
        else:
            self.errors = resp['errors']
        return False

    def get_exchange_rate(self, post_data):
        post_data.update({'action': 'getExchangeRate'})
        return self.get_response(post_data)
