### [Payeer.com](http://goo.gl/ewjZaO) API Python class ###

*Реализация cpayeer.php > CPayeer класса на Python 3.4*


```
#!python

from Payeer import PayeerAPI
```


**Авторизация**

```
#!python

payeer = PayeerAPI(account='P11111111', api_id='11111111', api_key='99999999')
if payeer.is_auth():
    print('Вы успешно авторизованы')
else:
    print(payeer.get_errors())
```

**Проверка баланса**

```
#!python

payeer = PayeerAPI(account='P11111111', api_id='11111111', api_key='99999999')
if payeer.is_auth():
    balance = payeer.get_balance()
    print(balance)
else:
    print(payeer.get_errors())
```
**Получение доступных платежных систем**

```
#!python

payeer = PayeerAPI(account='P11111111', api_id='11111111', api_key='99999999')
if payeer.is_auth():
    pay_systems = payeer.get_pay_systems()
    print(pay_systems)
else:
    print(payeer.get_errors())
```
**Выплата**

```
#!python

payeer = PayeerAPI(account='P11111111', api_id='11111111', api_key='99999999')
if payeer.is_auth():
    init_output = payeer.init_output({'ps': '1136053',
                                      'curIn': 'USD',
                                      'sumOut': 1,
                                      'curOut': 'USD',
                                      'param_ACCOUNT_NUMBER': 'P11111111'})
    if init_output:
        history_id = payeer.output():
        if history_id > 0:
            print('Выплата успешна')
        else:
            print(payeer.get_errors())
    else:
        print(payeer.get_errors())
else:
    print(payeer.get_errors())
```
**Информация об операции**

```
#!python

payeer = PayeerAPI(account='P11111111', api_id='11111111', api_key='99999999')
if payeer.is_auth():
    history = payeer.get_history_info(history_id=123456)
    print(history)
else:
    print(payeer.get_errors())
```
**Информация об операции в магазине**

```
#!python

payeer = PayeerAPI(account='P11111111', api_id='11111111', api_key='99999999')
if payeer.is_auth():
    shop_history = payeer.get_shop_order_info({'shopId': 'Shop id', 'orderId': 'Shop order id'})
    print(shop_history)
else:
    print(payeer.get_errors())
```
**Перевод средств**

```
#!python

payeer = PayeerAPI(account='P11111111', api_id='11111111', api_key='99999999')
if payeer.is_auth():
    transfer = payeer.transfer({'curIn': 'USD',
                                    'sum': 1,
                                    'curOut': 'USD',
                                    'to': 'transfer_email@example.com or phone or P11111111',
                                    # 'comment': 'test',
                                    # 'anonim': 'Y',
                                    # 'protect': 'Y',
                                    # 'protectPeriod': '3',
                                    # 'protectCode': '12345'
                                    })
    if not transfer.get('errors') and transfer.get('historyId') > 0:
        print('{}: Перевод средств успешно выполнен'.format(transfer.get('historyId')))
    else:
        print(transfer.get('errors'))
else:
    print(transfer.get_errors())
```
**Проверка существования аккаунта**

```
#!python

payeer = PayeerAPI(account='P11111111', api_id='11111111', api_key='99999999')
if payeer.is_auth():
    if payeer.check_user({'user': 'P11111111'}):
        print('exists')
    else:
        print('not found')
else:
    print(payeer.get_errors())
```
**Курсы конвертации**

```
#!python

payeer = PayeerAPI(account='P11111111', api_id='11111111', api_key='99999999')
if payeer.is_auth():
    input_exchange_rate = payeer.get_exchange_rate({'output': 'N'}):
    print(input_exchange_rate)
    output_exchange_rate = payeer.get_exchange_rate({'output': 'Y'}):
    print(output_exchange_rate)
else:
    print(payeer.get_errors())
```